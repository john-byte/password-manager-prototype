#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../../periphs/eeprom.h"
#include "../mocks/mock_utils.h"
#include "../../app/app.h"
#include "../../app/utils.h"
#include "../../app/consts.h"

static void check_begin_response() {
    UCHAR* usb_write_state = get_usb_socket_write_state();
    assert(usb_write_state[0] == RESPONSE_SIG_BEGIN);
}

static void check_end_response() {
    UCHAR* usb_write_state = get_usb_socket_write_state();

    assert(usb_write_state[RESPONSE_SIZE] == RESPONSE_SIG_END);

    USHORT key_id = int16_from_bytes_le(usb_write_state, RESPONSE_SIZE + 1);
    assert(key_id == 0);
}

static void check_eeprom() {
    UCHAR* eeprom_state = get_eeprom_state();

    UINT free_address = int32_from_bytes_le(
        eeprom_state, 
        EEPROM_FREE_ADDR_OFFSET
    );
    assert(free_address == (EEPROM_KEYS_OFFSET + KEY_SIZE));

    USHORT next_key_id = int16_from_bytes_le(
        eeprom_state,
        EEPROM_LAST_KEY_ID_OFFSET
    );
    assert(next_key_id == 1);

    UCHAR real_key[KEY_SIZE];
    memcpy(
        real_key,
        eeprom_state + EEPROM_KEYS_OFFSET,
        KEY_SIZE
    );
    UCHAR expected_key[KEY_SIZE];
    memset(expected_key, 0, KEY_SIZE);
    memcpy(expected_key, "aaabbbcccx", 10);
    for (UINT i = 0; i < KEY_SIZE; i++) {
        assert(real_key[i] == expected_key[i]);
    }
}

int main() {
    eeprom_init();
    usb_sock_init();

    app_init();
    
    app_loop();
    check_begin_response();

    app_loop();
    check_end_response();

    check_eeprom();

    printf("\n[create_key] Test succeed!\n");
    return 0;
}
