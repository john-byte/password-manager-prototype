#include <string.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../mocks/mock_utils.h"
#include "../../app/consts.h"
#include "../../app/utils.h"

static UCHAR g_read_mock[USB_SOCK_MOCK_SIZE];
static UINT g_read_ptr = 0;
static UCHAR g_write_mock[USB_SOCK_MOCK_SIZE];
static UINT g_write_ptr = 0;

static void setup_read_mock(UCHAR signal, UINT offset) {
    g_read_mock[offset] = signal;

    UINT pid = 1000;
    bytes_from_int_le(g_read_mock, 4, offset + 1, pid);

    UINT secret = 2222;
    bytes_from_int_le(g_read_mock, 4, offset + 5, secret);

    g_read_mock[offset + 9] = VERB_CREATE_KEY;

    memcpy(g_read_mock + offset + 10, "aaabbbcccx", 10);
}

void usb_sock_init() {
    memset(g_read_mock, 0, USB_SOCK_MOCK_SIZE);
    setup_read_mock(SIGNAL_TWEAK, 0);
    setup_read_mock(SIGNAL_COMMAND, REQUEST_SIZE);
    memset(g_write_mock, 0, USB_SOCK_MOCK_SIZE);
}

void usb_sock_read_byte(UCHAR* buffer) {
    buffer[0] = g_read_mock[g_read_ptr++];
}

void usb_sock_read_bytes(UCHAR* buffer, USHORT count) {
    memcpy(buffer, g_read_mock + g_read_ptr, count);
    g_read_ptr += (UINT)count;
}

void usb_sock_write_byte(const UCHAR* buffer) {
    g_write_mock[g_write_ptr++] = buffer[0];
}

void usb_sock_write_bytes(const UCHAR* buffer, USHORT count) {
    memcpy(g_write_mock + g_write_ptr, buffer, count);
    g_write_ptr += (UINT)count;
}

UCHAR* get_usb_socket_read_state() {
    return g_read_mock;
}

UCHAR* get_usb_socket_write_state() {
    return g_write_mock;
}
