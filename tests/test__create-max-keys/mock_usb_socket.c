#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../mocks/mock_utils.h"
#include "../../app/consts.h"
#include "../../app/utils.h"

#define USB_SOCK_MOCK_SIZE (1 << 18)
#define MIN_CHAR '0'
#define MAX_CHAR 'z'

static UCHAR g_read_mock[USB_SOCK_MOCK_SIZE];
static UINT g_read_ptr = 0;
static UCHAR g_write_mock[USB_SOCK_MOCK_SIZE];
static UINT g_write_ptr = 0;

static void generate_key(UCHAR* output) {
    srand(time(0));

    for (int i = 0; i < KEY_SIZE; i++) {
        UCHAR ch = rand() % (MAX_CHAR - MIN_CHAR + 1) + MIN_CHAR;
        output[i] = ch;
    }
}

static void setup_read_mock(UCHAR signal, UINT offset) {
    g_read_mock[offset + REQUEST_SIG_OFFSET] = signal;

    UINT pid = 1000;
    bytes_from_int_le(g_read_mock, REQUEST_PID_SIZE, offset + REQUEST_PID_OFFSET, pid);

    UINT secret = 2222;
    bytes_from_int_le(g_read_mock, REQUEST_SECRET_SIZE, offset + REQUEST_SECRET_OFFSET, secret);

    g_read_mock[offset + REQUEST_ACTION_OFFSET] = VERB_CREATE_KEY;

    UCHAR key[REQUEST_PAYLOAD_SIZE];
    memset(key, 0, REQUEST_PAYLOAD_SIZE);
    generate_key(key);
    memcpy(g_read_mock + offset + REQUEST_PAYLOAD_OFFSET, key, KEY_SIZE);
}

static void init_requests() {
    UINT keys = MAX_KEYS_CAPACITY / KEY_SIZE;
    for (int i = 0; i < keys + 1; i++) {
        setup_read_mock(SIGNAL_TWEAK, i * 2 * REQUEST_SIZE);
        setup_read_mock(SIGNAL_COMMAND, (i * 2 + 1) * REQUEST_SIZE);
    }
}

void usb_sock_init() {
    memset(g_read_mock, 0, USB_SOCK_MOCK_SIZE);
    init_requests();
    memset(g_write_mock, 0, USB_SOCK_MOCK_SIZE);
}

void usb_sock_read_byte(UCHAR* buffer) {
    buffer[0] = g_read_mock[g_read_ptr++];
}

void usb_sock_read_bytes(UCHAR* buffer, USHORT count) {
    memcpy(buffer, g_read_mock + g_read_ptr, count);
    g_read_ptr += (UINT)count;
}

void usb_sock_write_byte(const UCHAR* buffer) {
    g_write_mock[g_write_ptr++] = buffer[0];
}

void usb_sock_write_bytes(const UCHAR* buffer, USHORT count) {
    memcpy(g_write_mock + g_write_ptr, buffer, count);
    g_write_ptr += (UINT)count;
}

UCHAR* get_usb_socket_read_state() {
    return g_read_mock;
}

UCHAR* get_usb_socket_write_state() {
    return g_write_mock;
}
