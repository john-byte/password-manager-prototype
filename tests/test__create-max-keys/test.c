#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../../periphs/eeprom.h"
#include "../mocks/mock_utils.h"
#include "../../app/app.h"
#include "../../app/utils.h"
#include "../../app/consts.h"

static void check_success_begin_response(UINT offset) {
    UCHAR* usb_write_state = get_usb_socket_write_state();
    printf("[TEST_ARGS] usb_write_state[%d] = %d\n", offset, usb_write_state[offset]);
    assert(usb_write_state[offset] == RESPONSE_SIG_BEGIN);
}

static void check_success_end_response(UINT offset, USHORT expected_key_id) {
    UCHAR* usb_write_state = get_usb_socket_write_state();

    printf("[TEST_ARGS] usb_write_state[%d] = %d\n", offset, usb_write_state[offset]);
    assert(usb_write_state[offset] == RESPONSE_SIG_END);

    USHORT key_id = int16_from_bytes_le(usb_write_state, offset + 1);
    printf("[TEST_ARGS] Key_id = %d, expected_key_id = %d\n", key_id, expected_key_id);
    assert(key_id == expected_key_id);
}

static void check_success_responses() {
    UINT keys_count = MAX_KEYS_CAPACITY / KEY_SIZE;
    for (UINT i = 0; i < keys_count; i++) {
        app_loop();
        check_success_begin_response(i * 2 * RESPONSE_SIZE);

        app_loop();
        check_success_end_response((i * 2 + 1) * RESPONSE_SIZE, (USHORT)i);
    }
}

// TODO: modify it to check macthes for generated keys
static void check_eeprom() {
    UINT keys_count = MAX_KEYS_CAPACITY / KEY_SIZE;
    UCHAR* eeprom_state = get_eeprom_state();
    UCHAR* usb_read_state = get_usb_socket_read_state();

    UINT free_address = int32_from_bytes_le(
        eeprom_state, 
        EEPROM_FREE_ADDR_OFFSET
    );
    UINT expected_free_address = EEPROM_KEYS_OFFSET + KEY_SIZE * keys_count;
    printf(
        "[TEST_ARGS] real free_address = %d, expected free_address = %d\n",
        free_address,
        expected_free_address
    );
    assert(free_address == expected_free_address);

    USHORT next_key_id = int16_from_bytes_le(
        eeprom_state,
        EEPROM_LAST_KEY_ID_OFFSET
    );
    printf(
        "[TEST_ARGS] real next_key_id = %d, expected next_key_id = %d\n",
        next_key_id,
        keys_count
    );
    assert(next_key_id == keys_count);

    for (UINT i = 0; i < keys_count; i++) {
        UCHAR real_key[KEY_SIZE + 1];
        real_key[KEY_SIZE] = '\0';
        memcpy(
            real_key,
            eeprom_state + EEPROM_KEYS_OFFSET + KEY_SIZE * i,
            KEY_SIZE
        );

        UCHAR expected_key[KEY_SIZE + 1];
        expected_key[KEY_SIZE] = '\0';
        memcpy(
            expected_key, 
            usb_read_state + REQUEST_SIZE * i + REQUEST_PAYLOAD_OFFSET, 
            KEY_SIZE
        );

        printf(
            "\n=========================\n[TEST_ARGS] i = %d, real key = \"%s\", expected key = \"%s\"\n================\n",
            i, real_key,
            expected_key
        );
        for (UINT j = 0; j < KEY_SIZE; j++) {
            assert(real_key[j] == expected_key[j]);
        }
    }
}

static void check_controller_err_response(int offset) {
    UCHAR* usb_write_state = get_usb_socket_write_state();

    printf(
        "[TEST_ARGS] usb_write_state[%d] = %d\n",
        offset,
        usb_write_state[offset]
    );
    assert(usb_write_state[offset] == RESPONSE_SIG_CONTR_ERR);

    printf(
        "[TEST_ARGS] usb_write_state[%d] = %d\n",
        offset + 1,
        usb_write_state[offset + 1]
    );
    assert(usb_write_state[offset + 1] == VERB_CREATE_KEY);

    printf(
        "[TEST_ARGS] usb_write_state[%d] = %d\n",
        offset + 2,
        usb_write_state[offset + 2]
    );
    assert(usb_write_state[offset + 2] == CONTR_ERR_CK_EEPROM_FULL);
}

static void check_fail_responses() {
    UINT keys_count = MAX_KEYS_CAPACITY / KEY_SIZE;

    app_loop();
    check_success_begin_response(keys_count * 2 * RESPONSE_SIZE);

    app_loop();
    check_controller_err_response((keys_count * 2 + 1) * RESPONSE_SIZE);
}

int main() {
    eeprom_init();
    usb_sock_init();

    app_init();
    check_success_responses();
    check_eeprom();
    check_fail_responses();

    printf("\n[create_max_keys] Test succeed!\n");
    return 0;
}
