#include <string.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../mocks/mock_utils.h"
#include "../../app/consts.h"
#include "../../app/utils.h"

static UCHAR g_read_mock[USB_SOCK_MOCK_SIZE];
static UINT g_read_ptr = 0;
static UCHAR g_write_mock[USB_SOCK_MOCK_SIZE];
static UINT g_write_ptr = 0;

#define MOCK_MESSAGE_SIZE 20 
static const UCHAR gc_mock_enc_message[MOCK_MESSAGE_SIZE] = "zyxwvutsrqponmlk";
static const UCHAR gc_mock_dec_message[MOCK_MESSAGE_SIZE] = "klmnopqrstuvwxyz";

static void setup_read_mock(UCHAR signal, UINT offset, UCHAR verb, UCHAR* message) {
    g_read_mock[offset + REQUEST_SIG_OFFSET] = signal;

    UINT pid = 1000;
    bytes_from_int_le(g_read_mock, REQUEST_PID_SIZE, offset + REQUEST_PID_OFFSET, pid);

    UINT secret = 2222;
    bytes_from_int_le(g_read_mock, REQUEST_SECRET_SIZE, offset + REQUEST_SECRET_OFFSET, secret);

    g_read_mock[offset + REQUEST_ACTION_OFFSET] = verb;
    UCHAR key_id[2] = {0, 0};
    memcpy(g_read_mock + offset + REQUEST_PAYLOAD_OFFSET, key_id, 2);
    memcpy(g_read_mock + offset + REQUEST_PAYLOAD_OFFSET + KEY_ID_SIZE, message, MOCK_MESSAGE_SIZE);
}

void usb_sock_init() {
    memset(g_read_mock, 0, USB_SOCK_MOCK_SIZE);
    setup_read_mock(SIGNAL_TWEAK, 0, VERB_ENCRYPT, gc_mock_enc_message);
    setup_read_mock(SIGNAL_COMMAND, REQUEST_SIZE, VERB_ENCRYPT, gc_mock_enc_message);
    setup_read_mock(SIGNAL_TWEAK, REQUEST_SIZE * 2, VERB_DECRYPT, gc_mock_dec_message);
    setup_read_mock(SIGNAL_COMMAND, REQUEST_SIZE * 3, VERB_DECRYPT, gc_mock_dec_message);
    memset(g_write_mock, 0, USB_SOCK_MOCK_SIZE);
}

void usb_sock_read_byte(UCHAR* buffer) {
    buffer[0] = g_read_mock[g_read_ptr++];
}

void usb_sock_read_bytes(UCHAR* buffer, USHORT count) {
    memcpy(buffer, g_read_mock + g_read_ptr, count);
    g_read_ptr += (UINT)count;
}

void usb_sock_write_byte(const UCHAR* buffer) {
    g_write_mock[g_write_ptr++] = buffer[0];
}

void usb_sock_write_bytes(const UCHAR* buffer, USHORT count) {
    memcpy(g_write_mock + g_write_ptr, buffer, count);
    g_write_ptr += (UINT)count;
}

UCHAR* get_usb_socket_read_state() {
    return g_read_mock;
}

UCHAR* get_usb_socket_write_state() {
    return g_write_mock;
}
