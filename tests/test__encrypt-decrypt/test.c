#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "../../types.h"
#include "../../periphs/usb_socket.h"
#include "../../periphs/eeprom.h"
#include "../mocks/mock_utils.h"
#include "../../app/app.h"
#include "../../app/utils.h"
#include "../../app/consts.h"

#define MESSAGE_SIZE 16

static const UCHAR expected_enc_message[MESSAGE_SIZE] = {
    0x1b, 0x1b, 0x1b, 0x13, 0x13, 0x13, 0x13, 0x1b,
    0x1b, 0x1b, 0x1b, 0x03, 0x03, 0x03, 0x03, 0x1b,
};

static const UCHAR expected_dec_message[MESSAGE_SIZE] = {
    0x0a, 0x0e, 0x0e, 0x0a, 0x0a, 0x16, 0x16, 0x1a,
    0x1a, 0x1e, 0x1e, 0x1a, 0x1a, 0x16, 0x16, 0x0a,
};

static void check_begin_response(UINT offset) {
    UCHAR* usb_write_state = get_usb_socket_write_state();
    assert(usb_write_state[offset] == RESPONSE_SIG_BEGIN);
}

static void check_end_response(UINT offset, UCHAR* expected_message) {
    UCHAR* usb_write_state = get_usb_socket_write_state();
    assert(usb_write_state[offset] == RESPONSE_SIG_END);
    assert(memcmp(usb_write_state + offset + 1, expected_message, MESSAGE_SIZE) == 0);
}

int main() {
    eeprom_init();
    usb_sock_init();

    app_init();
    
    app_loop();
    check_begin_response(0);
    app_loop();
    check_end_response(RESPONSE_SIZE, expected_enc_message);

    app_loop();
    check_begin_response(RESPONSE_SIZE * 2);
    app_loop();
    check_end_response(RESPONSE_SIZE * 3, expected_dec_message);

    printf("\n[encrypt-decrypt] Test succeed!\n");
    return 0;
}
