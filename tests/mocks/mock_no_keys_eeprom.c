#include <string.h>

#include "../../types.h"
#include "../../periphs/eeprom.h"
#include "./mock_utils.h"
#include "../../app/utils.h"

static UCHAR g_store[EEPROM_SIZE];

void eeprom_init() {
    memset(g_store, 0, EEPROM_SIZE);
    
    UCHAR free_addr[4];
    bytes_from_int_le(free_addr, 4, 0, EEPROM_KEYS_OFFSET);
    memcpy(g_store, free_addr, 4);
}

void eeprom_read_byte(UCHAR* buffer, UINT address) {
    buffer[0] = g_store[address];
}

void eeprom_read_bytes(UCHAR* buffer, UINT address, UINT count) {
    memcpy(buffer, g_store + address, count);
}

void eeprom_write_byte(const UCHAR* buffer, UINT address) {
    g_store[address] = buffer[0];
}

void eeprom_write_bytes(const UCHAR* buffer, UINT address, UINT count) {
    memcpy(g_store + address, buffer, count);
}

UCHAR* get_eeprom_state() {
    return g_store;
}
