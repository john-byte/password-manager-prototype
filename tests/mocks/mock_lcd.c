#include <stdio.h>
#include "../../types.h"
#include "../../periphs/lcd.h"

#define clear_screen() printf("\033[H\033[J")

void lcd_display_text(const UCHAR* text) {
    // clear_screen();
    printf(text);
}