#include <string.h>

#include "../../types.h"
#include "../../periphs/eeprom.h"
#include "./mock_utils.h"
#include "../../app/utils.h"
#include "../../app/consts.h"

#define MOCK_KEY_SIZE 20

static UCHAR g_store[EEPROM_SIZE];
static const UCHAR gc_mock_key[MOCK_KEY_SIZE] = "abcdefghijklmnop";

void eeprom_init() {
    memset(g_store, 0, EEPROM_SIZE);
    
    UCHAR free_addr[4];
    bytes_from_int_le(free_addr, EEPROM_FREE_ADDR_SIZE, 0, EEPROM_KEYS_OFFSET + KEY_SIZE);
    memcpy(g_store + EEPROM_FREE_ADDR_OFFSET, free_addr, EEPROM_FREE_ADDR_SIZE);

    UCHAR last_id[2] = {1, 0};
    memcpy(g_store + EEPROM_LAST_KEY_ID_OFFSET, last_id, EEPROM_LAST_KEY_ID_SIZE);

    memcpy(g_store + EEPROM_KEYS_OFFSET, gc_mock_key, MOCK_KEY_SIZE);
}

void eeprom_read_byte(UCHAR* buffer, UINT address) {
    buffer[0] = g_store[address];
}

void eeprom_read_bytes(UCHAR* buffer, UINT address, UINT count) {
    memcpy(buffer, g_store + address, count);
}

void eeprom_write_byte(const UCHAR* buffer, UINT address) {
    g_store[address] = buffer[0];
}

void eeprom_write_bytes(const UCHAR* buffer, UINT address, UINT count) {
    memcpy(g_store + address, buffer, count);
}

UCHAR* get_eeprom_state() {
    return g_store;
}
