#ifndef MOCK_UTILS_H
#define MOCK_UTILS_H

#include "../../types.h"

#define EEPROM_SIZE (1 << 16)
#define USB_SOCK_MOCK_SIZE 4096


UCHAR* get_eeprom_state();
UCHAR* get_usb_socket_read_state();
UCHAR* get_usb_socket_write_state();

#endif