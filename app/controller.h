#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../types.h"

UCHAR controller_create_key(UCHAR* request_payload, UCHAR* response);
UCHAR controller_encrypt(UCHAR* request_payload, UCHAR* response);
UCHAR controller_decrypt(UCHAR* request_payload, UCHAR* response);
UCHAR controller_not_found(UCHAR* response);

#endif
