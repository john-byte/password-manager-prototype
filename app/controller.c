#include <string.h>
#include <stdio.h>

#include "../types.h"
#include "controller.h"
#include "consts.h"
#include "utils.h"
#include "../periphs/eeprom.h"
#include "cipher.h"

UCHAR controller_create_key(UCHAR* request_payload, UCHAR* response) {
    UCHAR last_id[EEPROM_LAST_KEY_ID_SIZE];
    eeprom_read_bytes(last_id, EEPROM_LAST_KEY_ID_OFFSET, EEPROM_LAST_KEY_ID_SIZE);
    USHORT i_last_id = int16_from_bytes_le(last_id, 0);

    if (i_last_id == MAX_KEYS_COUNT) {
        response[0] = RESPONSE_SIG_CONTR_ERR;
        response[1] = VERB_CREATE_KEY;
        response[2] = CONTR_ERR_CK_EEPROM_FULL;
        return PROCESS_ERR;
    }

    UCHAR free_addr[EEPROM_FREE_ADDR_SIZE];
    eeprom_read_bytes(free_addr, EEPROM_FREE_ADDR_OFFSET, EEPROM_FREE_ADDR_SIZE);
    UINT i_free_addr = int32_from_bytes_le(free_addr, 0);

    eeprom_write_bytes(
        request_payload,
        i_free_addr,
        KEY_SIZE
    );
    
    response[0] = RESPONSE_SIG_END;
    memcpy(response + 1, last_id, KEY_ID_SIZE);

    UINT next_last_id = (UINT)(i_last_id + 1);
    bytes_from_int_le(last_id, EEPROM_LAST_KEY_ID_SIZE, 0, next_last_id);
    eeprom_write_bytes(last_id, EEPROM_LAST_KEY_ID_OFFSET, EEPROM_LAST_KEY_ID_SIZE);

    UINT next_free_addr = i_free_addr + KEY_SIZE;
    bytes_from_int_le(free_addr, EEPROM_FREE_ADDR_SIZE, 0, next_free_addr);
    eeprom_write_bytes(free_addr, EEPROM_FREE_ADDR_OFFSET, EEPROM_FREE_ADDR_SIZE);

    return PROCESS_OK;
}

static void read_key(UCHAR* request_payload, UCHAR* key) {
    USHORT key_id = int16_from_bytes_le(request_payload, 0);
    
    eeprom_read_bytes(
        key,
        EEPROM_KEYS_OFFSET + ((UINT)key_id) * KEY_SIZE,
        KEY_SIZE
    );
}

UCHAR controller_encrypt(UCHAR* request_payload, UCHAR* response) {
    static UCHAR key[KEY_SIZE];
    memset(key, 0, KEY_SIZE);
    read_key(request_payload, key);
    cipher_encrypt(request_payload + KEY_ID_SIZE, DATA_CHUNK_SIZE, key, KEY_SIZE);

    response[0] = RESPONSE_SIG_END;
    memcpy(response + 1, request_payload + KEY_ID_SIZE, DATA_CHUNK_SIZE);

    return PROCESS_OK;
}

UCHAR controller_decrypt(UCHAR* request_payload, UCHAR* response) {
    static UCHAR key[KEY_SIZE];
    memset(key, 0, KEY_SIZE);
    read_key(request_payload, key);
    cipher_decrypt(request_payload + KEY_ID_SIZE, DATA_CHUNK_SIZE, key, KEY_SIZE);

    response[0] = RESPONSE_SIG_END;
    memcpy(response + 1, request_payload + KEY_ID_SIZE, DATA_CHUNK_SIZE);

    return PROCESS_OK;
}

UCHAR controller_not_found(UCHAR* response) {
    // TODO: rework error protocol for controllers
    response[0] = RESPONSE_SIG_CONTR_ERR;
    response[1] = VERB_NOT_FOUND;
    response[2] = CONTR_ERR_NOT_FOUND;
    return PROCESS_ERR;
}