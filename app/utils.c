#include "../types.h"
#include "utils.h"

UINT int32_from_bytes_le(const UCHAR* bytes, UINT offset) {
    return ((UINT)(bytes[offset])) + (((UINT)(bytes[offset + 1])) << 8) +
        (((UINT)(bytes[offset + 2])) << 16) + (((UINT)(bytes[offset + 3])) << 24);
}

USHORT int16_from_bytes_le(const UCHAR* bytes, UINT offset) {
    return ((USHORT)(bytes[offset])) + (((USHORT)(bytes[offset + 1])) << 8);
}

void bytes_from_int_le(UCHAR* bytes, UINT count, UINT offset, UINT val) {
    for (UCHAR i = 0; i < count; i++) {
        bytes[offset + i] = (UCHAR)(val & 255);
        val >>= 8;
    }
}