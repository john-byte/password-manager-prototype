#include <string.h>
#include <stdio.h>

#include "../types.h"
#include "app.h"
#include "utils.h"
#include "consts.h"
#include "controller.h"
#include "../periphs/usb_socket.h"
#include "../periphs/lcd.h"
#include "../periphs/buttons.h"

#define STATE_IDLE 0
#define STATE_RECV 1
#define LCD_TEXT_SIZE 1024

static UCHAR g_request[REQUEST_SIZE];
static UCHAR g_response[RESPONSE_SIZE];
static UCHAR g_lcd_text[LCD_TEXT_SIZE];

static UCHAR g_state = STATE_IDLE;
static UINT g_pid = 0;
static UINT g_secret = 0;
static UCHAR g_action = 0;

static UCHAR g_calls = 0;

static UCHAR check_state(UCHAR state) {
    return g_state != state;
}

static void reset_state_chunks() {
    memset(g_request, 0, REQUEST_SIZE);
    memset(g_response, 0, RESPONSE_SIZE);
    memset(g_lcd_text, 0, LCD_TEXT_SIZE);
}

static void reset_state_header() {
    g_pid = 0;
    g_secret = 0;
    g_action = 0;
    g_state = 0;
    g_calls = 0;
}

static UCHAR* get_action_name(UCHAR action) {
    switch (action) {
        case VERB_CREATE_KEY:
            return "Create key";
        case VERB_ENCRYPT:
            return "Encrypt";
        case VERB_DECRYPT:
            return "Decrypt";
        default:
            return "Unknown";
    }
}

void app_init() {
    reset_state_chunks();
    reset_state_header();
};

static UCHAR handle_tweak_signal() {
    if (check_state(STATE_IDLE)) {
        reset_state_header();
    }

    UINT pid = int32_from_bytes_le(g_request, REQUEST_PID_OFFSET);
    UINT secret = int32_from_bytes_le(g_request, REQUEST_SECRET_OFFSET);
    UCHAR action = g_request[REQUEST_ACTION_OFFSET];

    sprintf(
        g_lcd_text,
        "Accept request for action \"%s\" from pid = %d with secret = %d? y/n",
        get_action_name(action),
        pid,
        secret
    );
    lcd_display_text(g_lcd_text);

    UCHAR resolved = 0, rejected = 0;
    while (!resolved && !rejected) {
        resolved = btns_read_pin(PIN_RESOLVE);
        rejected = btns_read_pin(PIN_REJECT);
    }

    lcd_display_text("");
    if (rejected) {
        g_response[0] = RESPONSE_SIG_APP_ERR;
        g_response[1] = ERR_REQUEST_REJECTED;
        return PROCESS_ERR;
    }

    g_response[0] = RESPONSE_SIG_BEGIN;
    g_state = STATE_RECV;
    g_pid = pid;
    g_secret = secret;
    g_action = action;
    return PROCESS_OK;
}

static UCHAR handle_command_signal() {
    if (check_state(STATE_RECV)) {
        g_response[0] = RESPONSE_SIG_APP_ERR;
        g_response[1] = ERR_INVALID_SIGNAL;
        return PROCESS_ERR;
    }

    UINT pid = int32_from_bytes_le(g_request, REQUEST_PID_OFFSET);
    UINT secret = int32_from_bytes_le(g_request, REQUEST_SECRET_OFFSET);
    UCHAR action = g_request[REQUEST_ACTION_OFFSET];

    if (pid != g_pid || secret != g_secret || action != g_action) {
        g_response[0] = RESPONSE_SIG_APP_ERR;
        g_response[1] = ERR_INVALID_REQ_HEADER;
        return PROCESS_ERR;
    }

    UCHAR err = PROCESS_OK;
    switch (action) {
        case VERB_CREATE_KEY:
            err = controller_create_key(
                g_request + REQUEST_PAYLOAD_OFFSET,
                g_response
            );
            break;
        case VERB_ENCRYPT:
            err = controller_encrypt(
                g_request + REQUEST_PAYLOAD_OFFSET,
                g_response
            );
            break;
        case VERB_DECRYPT:
            err = controller_decrypt(
                g_request + REQUEST_PAYLOAD_OFFSET,
                g_response
            );
            break;
        default:
            err = controller_not_found(g_response);
            break;
    }

    if (err == PROCESS_OK) {
        g_calls++;
        if (g_calls == MAX_CALLS) {
          reset_state_header();
        }
    }
    return err;
}

static UCHAR handle_not_found_signal() {
    g_response[0] = RESPONSE_SIG_APP_ERR;
    g_response[1] = ERR_UNKNOWN_SIGNAL;
    return PROCESS_ERR;
}

void app_loop() {
    reset_state_chunks();
    usb_sock_read_bytes(g_request, REQUEST_SIZE);
    UCHAR sig = g_request[REQUEST_SIG_OFFSET];
    UCHAR err = PROCESS_OK;

    switch (sig) {
        case SIGNAL_TWEAK:
            err = handle_tweak_signal();
            break;
        case SIGNAL_COMMAND:
            err = handle_command_signal();
            break;
        default:
            err = handle_not_found_signal();
            break;
    }

    usb_sock_write_bytes(g_response, RESPONSE_SIZE);

    if (err) {
        reset_state_header();
    }
};