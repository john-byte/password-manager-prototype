#include "../types.h"
#include "cipher.h"

static void xor_cipher(UCHAR* data, UINT data_len, const UCHAR* key, UINT key_len) {
    for (UINT i = 0; i < data_len; i++) {
        data[i] = data[i] ^ key[i % key_len];
    }
}

void cipher_encrypt(UCHAR* data, UINT data_len, const UCHAR* key, UINT key_len) {
    xor_cipher(data, data_len, key, key_len);
}

void cipher_decrypt(UCHAR* data, UINT data_len, const UCHAR* key, UINT key_len) {
    xor_cipher(data, data_len, key, key_len);
}