#ifndef CIPHER_H
#define CIPHER_H

#include "../types.h"

void cipher_encrypt(UCHAR* data, UINT data_len, const UCHAR* key, UINT key_len);
void cipher_decrypt(UCHAR* data, UINT data_len, const UCHAR* key, UINT key_len);

#endif