#ifndef UTILS_H
#define UTILS_H

#include "../types.h"

UINT int32_from_bytes_le(const UCHAR* bytes, UINT offset);
USHORT int16_from_bytes_le(const UCHAR* bytes, UINT offset);
void bytes_from_int_le(UCHAR* bytes, UINT count, UINT offset, UINT val);

#endif
