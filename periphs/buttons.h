#ifndef BUTTONS_H
#define BUTTONS_H

#include "../types.h"

#define PIN_RESOLVE 0
#define PIN_REJECT 1

UCHAR btns_read_pin(UCHAR pin_id);

#endif