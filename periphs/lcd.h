#ifndef LCD_H
#define LCD_H

#include "../types.h"

void lcd_init();
void lcd_display_text(const UCHAR* text);

#endif
