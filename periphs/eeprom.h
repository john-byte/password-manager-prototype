#ifndef EEPROM_H
#define EEPROM_H

#include "../types.h"

#define EEPROM_FREE_ADDR_OFFSET 0
#define EEPROM_FREE_ADDR_SIZE 4
#define EEPROM_LAST_KEY_ID_OFFSET 4
#define EEPROM_LAST_KEY_ID_SIZE 2
#define EEPROM_KEYS_OFFSET 6
#define EEPROM_KEYS_SIZE (256 * 256)

void eeprom_init();
void eeprom_read_byte(UCHAR* buffer, UINT address);
void eeprom_read_bytes(UCHAR* buffer, UINT address, UINT count);
void eeprom_write_byte(const UCHAR* buffer, UINT address);
void eeprom_write_bytes(const UCHAR* buffer, UINT address, UINT count);

#endif
