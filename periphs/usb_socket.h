#ifndef USB_SOCKET_H
#define USB_SOCKET_H

#include "../types.h"

void usb_sock_init();
void usb_sock_read_byte(UCHAR* buffer);
void usb_sock_read_bytes(UCHAR* buffer, USHORT count);
void usb_sock_write_byte(const UCHAR* buffer);
void usb_sock_write_bytes(const UCHAR* buffer, USHORT count);

#endif
